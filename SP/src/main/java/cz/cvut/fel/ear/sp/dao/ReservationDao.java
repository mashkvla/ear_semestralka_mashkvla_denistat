package cz.cvut.fel.ear.sp.dao;

import cz.cvut.fel.ear.sp.model.Game;
import cz.cvut.fel.ear.sp.model.Reservation;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

import java.util.List;
import java.util.Objects;

public class ReservationDao extends DaoBase<Reservation>{

    public ReservationDao(){
        super(Reservation.class);
    }

    @PersistenceContext
    private EntityManager em;

    public Reservation findReservation(Integer reservationId) {
        Objects.requireNonNull(reservationId);
        return em.find(Reservation.class, reservationId);
    }

    public List<Reservation> showAllReservations() {
        return em.createNamedQuery("Reservation.showAllReservations", Reservation.class)
                .getResultList();
    }


}
