package cz.cvut.fel.ear.sp.model;

import jakarta.persistence.*;

import java.util.Date;
import java.util.List;

@Entity
@Table(name = "Reservation")
@NamedQueries({
        @NamedQuery(name = "Reservation.showAllReservations", query = "SELECT r FROM Reservation r")
})
public class Reservation {
    @Id
    @GeneratedValue
    private Integer reservationId;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Student student;

    @OneToMany(cascade = CascadeType.PERSIST, orphanRemoval = true)
    @JoinColumn(name = "reservationId")
    private List<Game> games;

    @Column
    private Date startTime;

    @Column
    private Date endTime;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    //GETTERS


    public Reservation() {
        this.status = Status.WAITING;
    }

    public Integer getReservationId() {
        return reservationId;
    }
    public Student getStudent() {
        return student;
    }
    public List<Game> getGames() {
        return games;
    }
    public Date getStartTime() {
        return startTime;
    }
    public Date getEndTime() {
        return endTime;
    }
    public Status getStatus() {
        return status;
    }
    //SETTERS

    public void setReservationId(Integer reservationId) {
        this.reservationId = reservationId;
    }
    public void setReservationId(int reservationId) {
        this.reservationId = reservationId;
    }
    public void setStudent(Student user) {
        this.student = user;
    }
    public void setGames(List<Game> game) {
        this.games = game;
    }
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
    public void setStatus(Status status) {
        this.status = status;
    }
}