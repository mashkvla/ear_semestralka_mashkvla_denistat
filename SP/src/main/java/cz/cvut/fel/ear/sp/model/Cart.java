package cz.cvut.fel.ear.sp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.cvut.fel.ear.sp.dao.CartDao;
import jakarta.persistence.*;

import java.util.List;

@Entity
public class Cart{

    @Id
    @GeneratedValue
    private Integer cartId;

    @JsonIgnore
    @OneToOne(mappedBy = "cart", optional = false)
    private Student cartOwner;

    @OneToMany(cascade = CascadeType.MERGE, orphanRemoval = true)
    @JoinColumn(name = "cartId")
    private List<Game> games;

//    public CartDao cartDao;


    //GETTERS
    public User getCartOwner() {
        return cartOwner;
    }
    public List<Game> getGames() {
        return games;
    }
    public Integer getCartId() {
        return cartId;
    }

    //SETTERS
    public void setCartOwner(Student cartOwner) {
        this.cartOwner = cartOwner;
    }
    public void setGames(List<Game> games) {
        this.games = games;
    }
    public void setCartId(Integer cartId) {
        this.cartId = cartId;
    }

    public void addGame(Game game){
        games.add(game);

    }

    public void checkout(){

    }
}