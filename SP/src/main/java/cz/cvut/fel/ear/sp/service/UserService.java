package cz.cvut.fel.ear.sp.service;

import cz.cvut.fel.ear.sp.dao.GameDao;
import cz.cvut.fel.ear.sp.dao.UserDao;
import cz.cvut.fel.ear.sp.model.*;
import org.springframework.security.crypto.password.PasswordEncoder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Service
public class UserService {

    private final UserDao userDao;
    private final GameDao gameDao;

    private final PasswordEncoder passwordEncoder;

    final Student currentUser = new Student(); // singleton simulating logged-in user

    @Autowired
    public UserService(UserDao userDao, GameDao gameDao, PasswordEncoder passwordEncoder) {
        this.userDao = userDao;
        this.passwordEncoder = passwordEncoder;
        this.gameDao = gameDao;
    }

    //FUNCTIONS

    @Transactional
    public void save(Student student) {
        Objects.requireNonNull(student);
        student.encodePassword(passwordEncoder);
        createCart(student);
        userDao.save(student);
    }

    public boolean exists(String email) {
        return userDao.findUserByEmail(email) != null;
    }

    private void createCart(Student student) {
        final Cart cart = new Cart();
        student.setCart(cart);
        cart.setCartOwner(student);
    }

    public void login(String email, String password){

    }

    public List<String> viewProfile(Student student){
        if(exists(student.getEmail())) {
            return Arrays.asList(
                    student.getName(),
                    student.getSurname(),
                    student.getUsername(),
                    student.getEmail());
        }
        return null;
    }

    public List<Game> viewAvailableGames(){
        return gameDao.showAvailableGames();
    }

    public List<Serializable> viewGameDetails(Game game){
        return Arrays.asList(
                game.getTitle(),
                game.getDescription(),
                game.getAgeRestriction(),
                game.getMinMembers(),
                game.getMaxMembers(),
                game.getTotalQuantity(),
                gameDao.showCategories(game).toArray()
        );
    }

    //TODO: create cart

    public void addToCart(String username){

    }

    public void viewCart(String username){

    }

    private void makeReservation(){

    }

    private void removeReservation(){

    }

    private List<Reservation> viewReservations(){
        return null;
    }
}
