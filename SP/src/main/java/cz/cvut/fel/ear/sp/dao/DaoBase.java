package cz.cvut.fel.ear.sp.dao;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

public abstract class DaoBase<T> implements GenericDao<T>{

    //The @PersistenceContext annotation indicates that the em field
    //should be populated by the container with managed entities.
    //This means that the container will automatically create and
    //close EntityManager instances as needed.
    @PersistenceContext
    protected EntityManager em;

    //The type field stores the class of objects that the DAO manages.
    //This field is used to define the type of objects to be returned by DAO methods.
    protected final Class<T> classType;

    //The DaoBase() constructor takes the class of objects that the DAO manages as a parameter.
    //This parameter is used to set the type field.
    protected DaoBase(Class<T> type) {
        this.classType = type;
    }

    @Override
    public T findById(Integer id) {
        Objects.requireNonNull(id);
        return em.find(classType, id);
    }

    @Override
    public List<T> findAll() {
        return em.createQuery("SELECT q FROM " + classType.getSimpleName() + " q", classType).getResultList();
    }

    @Override
    public void save(T entity) {
        Objects.requireNonNull(entity);
        em.persist(entity);
    }

    @Override
    public void save(Collection<T> entities) {
        Objects.requireNonNull(entities);
        if(entities.isEmpty())
            return;
        entities.forEach(this::save);
    }

    @Override
    public T update(T entity) {
        Objects.requireNonNull(entity);
        return em.merge(entity);
    }

    @Override
    public void delete(T entity) {
        Objects.requireNonNull(entity);
        final T toDelete = em.merge(entity);
        if(toDelete != null)
            em.remove(entity);
    }

    @Override
    public boolean exists(Integer id) {
        return id != null && em.find(classType, id) != null;
    }
}
