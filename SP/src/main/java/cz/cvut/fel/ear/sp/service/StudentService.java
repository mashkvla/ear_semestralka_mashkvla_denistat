package cz.cvut.fel.ear.sp.service;

import cz.cvut.fel.ear.sp.dao.UserDao;
import cz.cvut.fel.ear.sp.model.Student;
import org.springframework.stereotype.Service;

@Service
public class StudentService {

    private UserDao userDao;

    //FUNCTIONS
    public Student register(String username, String password, String name, String surname, String email){
        Student student;
        student = new Student(username, password, name, surname, email);
        userDao.save(student);
        return student;
    }

    public void makeAdmin(String studentEmail){
        userDao.findUserByEmail(studentEmail).setAdmin(true);
    }
}
