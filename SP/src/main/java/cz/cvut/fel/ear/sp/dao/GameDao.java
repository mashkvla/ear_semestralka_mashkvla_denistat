package cz.cvut.fel.ear.sp.dao;

import cz.cvut.fel.ear.sp.model.Game;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Repository
public class GameDao extends DaoBase<Game>{

    @PersistenceContext
    private EntityManager em;

    protected GameDao() {
        super(Game.class);
    }

    public Game findGame(Integer gameId) {
        Objects.requireNonNull(gameId);
        return em.find(Game.class, gameId);
    }

    public List<String> showCategories(Game game) {
        if (game != null) {
            List<String> gameCategories = new ArrayList<>();
            for(int i = 0; i < game.getCategories().size(); i++)
                gameCategories.add(game.getCategories().get(i).getName());
            return gameCategories;
        }
        return null;
    }


    public List<Game> showAvailableGames() {
        return em.createNamedQuery("Game.showAllAvailableGames", Game.class).setParameter("status", true)
                .getResultList();
    }
}
