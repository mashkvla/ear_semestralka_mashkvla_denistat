package cz.cvut.fel.ear.sp.service;

import cz.cvut.fel.ear.sp.dao.ReservationDao;
import cz.cvut.fel.ear.sp.model.Game;
import cz.cvut.fel.ear.sp.model.Reservation;
import cz.cvut.fel.ear.sp.model.Status;
import cz.cvut.fel.ear.sp.model.Student;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class AdminService {

    private Game game;

    private ReservationDao reservationDao;

    //FUNCTIONS
    //TODO
    private void updateGame(Game game){
        this.game = game;
    }

    private void approveReservation(Reservation reservation){
        Objects.requireNonNull(reservation);
        if(reservation.getStatus() == Status.WAITING)
            reservation.setStatus(Status.APPROVED);
    }

    private void rejectReservation(Reservation reservation){
        Objects.requireNonNull(reservation);
        if(reservation.getStatus() != Status.APPROVED)
            reservation.setStatus(Status.REJECTED);
    }

    private Student trackGameLocation(Integer gameId){
        List<Reservation> allReservations = reservationDao.showAllReservations();
        for (Reservation allReservation : allReservations) {
            List<Game> reservedGames = allReservation.getGames();
            for (Game reservedGame : reservedGames) {
                if (Objects.equals(reservedGame.getGameID(), gameId))
                    return allReservation.getStudent();
            }
        }
        return null ;
    }


}
