package cz.cvut.fel.ear.sp.dao;

import cz.cvut.fel.ear.sp.model.Cart;
import org.springframework.stereotype.Repository;

@Repository
public class CartDao extends DaoBase<Cart> {
    protected CartDao() {
        super(Cart.class);
    }
}
