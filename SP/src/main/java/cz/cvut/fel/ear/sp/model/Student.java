package cz.cvut.fel.ear.sp.model;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;

@Entity
@DiscriminatorValue(value = "student")
public class Student extends User {

    public Student() {}

    public Student(String username, String password, String name, String surname, String email) {
        super(username, password, name, surname, email);
    }


    public void initStudent(String username, String password, String name, String surname, String email){
        Student student = new Student(username, password, name, surname, email);
    }
}
