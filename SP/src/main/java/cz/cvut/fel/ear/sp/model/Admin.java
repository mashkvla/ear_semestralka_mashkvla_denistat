package cz.cvut.fel.ear.sp.model;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;

@Entity
@DiscriminatorValue(value = "admin")
public class Admin extends User{

}
