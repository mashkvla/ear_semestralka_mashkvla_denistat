package cz.cvut.fel.ear.sp.model;

import jakarta.persistence.*;

@Entity
public class Category {

    @Id
    @GeneratedValue
    private int categoryId;

    @Basic(optional = false)
    @Column(nullable = false)
    private String name;

    //SETTERS
    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
    public void setName(String name) {
        this.name = name;
    }

    //GETTERS
    public int getCategoryId() {
        return categoryId;
    }
    public String getName() {
        return name;
    }

    //TO STRING
    @Override
    public String toString() {
        return "Category{" +
                "categoryId=" + categoryId +
                ", name='" + name + '\'' +
                '}';
    }
}