package cz.cvut.fel.ear.sp.dao;

import cz.cvut.fel.ear.sp.model.User;
import org.springframework.stereotype.*;

@Repository
public class UserDao extends DaoBase<User>{

    protected UserDao() {
        super(User.class);
    }

    public User findUserByEmail(String email){
        return em.createNamedQuery("User.findUserByEmail", User.class).setParameter("email", email).getSingleResult();
    }
}
