package cz.cvut.fel.ear.sp.model;

public enum Status {
    APPROVED("RESERVATION_APPROVED"), WAITING("NOT_ACCEPTED_YET"), REJECTED("RESERVATION_REJECTED");

    private final String status;

    Status(String status) {this.status = status;}

    @Override
    public String toString() {
        return status;
    }
}
