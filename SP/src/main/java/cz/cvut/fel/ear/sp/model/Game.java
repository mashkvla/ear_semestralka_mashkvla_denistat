package cz.cvut.fel.ear.sp.model;

import jakarta.persistence.*;

import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name = "Game.showAllAvailableGames", query = "SELECT g FROM Game g WHERE g.isAvailable = :status ")
})
public class Game {

    @Id
    @GeneratedValue
    private int gameID;

    @Basic(optional = false)
    @Column(name = "title", nullable = false)
    private String title;

    @Basic(optional = false)
    @Column(name = "description", nullable = false)
    private String description;

    @Basic(optional = false)
    @Column(name = "totalQuantity", nullable = false)
    private Integer totalQuantity;

    @Basic(optional = false)
    @Column(name = "ageRestriction", nullable = false)
    private Integer ageRestriction;

    @Basic(optional = false)
    @Column(name = "minMembers", nullable = false)
    private Integer minMembers;

    @Basic(optional = false)
    @Column(name = "maxMembers", nullable = false)
    private Integer maxMembers;

    @Basic(optional = false)
    @Column(name = "avalibility", nullable = false)
    private boolean isAvailable;

    //TODO{
    // Read about those @
    // }
    @ManyToMany(fetch = FetchType.EAGER)
    @OrderBy("name")
    @JoinTable(name = "game_categoryAsdaaewea")
    private List<Category> categories;

    //GETTERS
    public Integer getGameID() {
        return gameID;
    }
    public String getTitle() {
        return title;
    }
    public String getDescription() {
        return description;
    }
    public Integer getTotalQuantity() {
        return totalQuantity;
    }
    public Integer getAgeRestriction() {
        return ageRestriction;
    }
    public Integer getMinMembers() {
        return minMembers;
    }
    public Integer getMaxMembers() {
        return maxMembers;
    }
    public boolean isAvailable() {
        return isAvailable;
    }
    public List<Category> getCategories() {
        return categories;
    }

    //SETTERS
    public void setGameID(Integer gameID) {
        this.gameID = gameID;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setTotalQuantity(Integer totalQuantity) {
        this.totalQuantity = totalQuantity;
    }
    public void setAgeRestriction(Integer ageRestriction) {
        this.ageRestriction = ageRestriction;
    }
    public void setMinMembers(Integer minMembers) {
        this.minMembers = minMembers;
    }
    public void setMaxMembers(Integer maxMembers) {
        this.maxMembers = maxMembers;
    }
    public void setAvailable(boolean available) {
        isAvailable = available;
    }
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }
}