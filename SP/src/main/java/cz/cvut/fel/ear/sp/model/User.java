package cz.cvut.fel.ear.sp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

@MappedSuperclass
@Table(name = "allUsers")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type")
@DiscriminatorValue(value = "user")
@NamedQueries({
        @NamedQuery(name = "User.findUserByEmail", query = "SELECT u FROM Student u WHERE u.email = :email")
})
public abstract class User{

    public User() {
    }

    public User(String username, String password, String name, String surname, String email) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.isAdmin = false;
    }

    @Id
    @GeneratedValue
    private Integer userId;

    @Basic(optional = false)
    @Column(name = "username", nullable = false)
    private String username;

    @Basic
    @Column(name = "is_admin")
    private boolean isAdmin;

    @Basic(optional = false)
    @Column(name = "password", nullable = false)
    private String password;

    @Basic(optional = false)
    @Column(name = "name", nullable = false)
    private String name;

    @Basic(optional = false)
    @Column(name = "surname", nullable = false)
    private String surname;

    @Basic(optional = false)
    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @JsonIgnore
    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Cart cart;

    @OneToMany(cascade = {CascadeType.PERSIST})
    private List<Reservation> reservations;

    //GETTERS
    public String getUsername() {
        return username;
    }
    public String getPassword() {
        return password;
    }
    public String getName() {
        return name;
    }
    public String getSurname() {
        return surname;
    }
    public String getEmail() {
        return email;
    }
    public Cart getCart() {
        return cart;
    }
    public List<Reservation> getReservations() {
        return reservations;
    }
    public Integer getUserId() {
        return userId;
    }

    //SETTERS
    public void setUsername(String username) {
        this.username = username;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setCart(Cart cart) {
        this.cart = cart;
    }
    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }
    public void setUserId(Integer userId) {
        this.userId = userId;
    }
    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public void encodePassword(PasswordEncoder encoder) {
        this.password = encoder.encode(password);
    }

    public void erasePassword() {
        this.password = null;
    }

    public boolean isAdmin() {
        return isAdmin;
    }
}