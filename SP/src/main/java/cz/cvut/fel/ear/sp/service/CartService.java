package cz.cvut.fel.ear.sp.service;

import cz.cvut.fel.ear.sp.dao.CartDao;
import cz.cvut.fel.ear.sp.dao.GameDao;
import cz.cvut.fel.ear.sp.dao.ReservationDao;
import cz.cvut.fel.ear.sp.model.Cart;
import cz.cvut.fel.ear.sp.model.Game;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CartService {

    private Cart cart;
    private CartDao cartDao;
    private GameDao gameDao;
    private ReservationDao reservationDao;


    //FUNCTIONS

    public List<Game> showGames(){
        return cart.getGames();
    }

    private void removeAll(){

    }

    private void removeGame(Game game){

    }
}
