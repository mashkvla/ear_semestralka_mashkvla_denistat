package cz.cvut.fel.ear.sp.service;

import cz.cvut.fel.ear.sp.dao.GameDao;
import cz.cvut.fel.ear.sp.dao.UserDao;
import cz.cvut.fel.ear.sp.environment.Generator;
import cz.cvut.fel.ear.sp.model.Student;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;

/**
 * This test does not use Spring.
 * <p>
 * It showcases how services can be unit tested without being dependent on the application framework or database.
 */
@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Mock
    private UserDao userDaoMock;

    private UserService userService;
    private GameDao gameDao;

    @BeforeEach
    public void setUp() {
        this.userService = new UserService(userDaoMock, gameDao, passwordEncoder);
    }

    @Test
    public void saveEncodesUserPassword() {
        final Student student = Generator.generateStudent();
        final String studentPassword = student.getPassword();
        userService.save(student);

        final ArgumentCaptor<Student> captor = ArgumentCaptor.forClass(Student.class);
        verify(userDaoMock).save(captor.capture());
        assertTrue(passwordEncoder.matches(studentPassword, captor.getValue().getPassword()));
    }

//    @Test
//    public void persistCreatesCartForRegularUser() {
//        final User user = Generator.generateUser();
//        user.setRole(Role.USER);
//        sut.persist(user);
//
//        final ArgumentCaptor<User> captor = ArgumentCaptor.forClass(User.class);
//        verify(userDaoMock).persist(captor.capture());
//        assertNotNull(captor.getValue().getCart());
//    }
//
//    @Test
//    public void persistCreatesCartForGuestUser() {
//        final User user = Generator.generateUser();
//        user.setRole(Role.GUEST);
//        sut.persist(user);
//
//        final ArgumentCaptor<User> captor = ArgumentCaptor.forClass(User.class);
//        verify(userDaoMock).persist(captor.capture());
//        assertNotNull(captor.getValue().getCart());
//    }
//
//    @Test
//    public void persistSetsUserRoleToDefaultWhenItIsNotSpecified() {
//        final User user = Generator.generateUser();
//        user.setRole(null);
//        sut.persist(user);
//
//        final ArgumentCaptor<User> captor = ArgumentCaptor.forClass(User.class);
//        verify(userDaoMock).persist(captor.capture());
//        assertEquals(Role.USER, captor.getValue().getRole());
//    }
}

