package cz.cvut.fel.ear.sp.environment;

//import cz.cvut.fel.ear.sp.model.;
import cz.cvut.fel.ear.sp.model.Student;

import java.util.Random;

public class Generator {

    private static final Random RAND = new Random();

    public static int randomInt() {
        return RAND.nextInt();
    }

    public static int randomInt(int max) {
        return RAND.nextInt(max);
    }

    public static int randomInt(int min, int max) {
        assert min >= 0;
        assert min < max;

        int result;
        do {
            result = randomInt(max);
        } while (result < min);
        return result;
    }

    public static boolean randomBoolean() {
        return RAND.nextBoolean();
    }

    public static Student generateStudent() {
        final Student student = new Student();
        student.setName("Name" + randomInt());
        student.setSurname("Surname" + randomInt());
        student.setUsername("username" + randomInt() + "@fel.cvut.cz");
        student.setPassword(Integer.toString(randomInt()));
        return student;
    }

//    public static Product generateProduct() {
//        final Product p = new Product();
//        p.setName("Product" + randomInt());
//        p.setAmount(1);
//        p.setPrice(1.0);
//        return p;
//    }
}
